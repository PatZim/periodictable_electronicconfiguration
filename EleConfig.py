
# --------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------
import sys
import random
# --------------------------------------------------------------------------------------------------------
def shellFull(shell,ele):
    if (shell == 's') and (ele == 2):
        return 1
    elif (shell == 'p') and (ele == 6):
        return 1
    elif (shell == 'd') and (ele == 10):
        return 1 
    elif (shell == 'f') and (ele == 14):
        return 1                
    else:
        return 0
# --------------------------------------------------------------------------------------------------------
def getConfigString(main,shells,shell,shellStr,eleShell):
    eleConfStr = ''
    if (main < len(shells)) and (shell < len(shells[main]) ):          
        # shellStr must be either s,p,d or f
        if shellStr == 's':    
            eleConfStr =  "%d" % (main+1) + shellStr + "%02d " % eleShell
        elif shellStr == 'p':    
            eleConfStr =  "%d" % (main+1) + shellStr + "%02d " % eleShell
        elif shellStr == 'd':    
            eleConfStr =  "%d" % (main) + shellStr + "%02d " % eleShell
        elif shellStr == 'f':
            eleConfStr =  "%d" % (main-1) + shellStr + "%02d " % eleShell
    return eleConfStr
# --------------------------------------------------------------------------------------------------------
def zToElem(z):
    # simple list with all element tags
    if (1<=z<=10):
        tableElem = ['H','He','Li','Be','B','C','N','O','F','Ne']  
        elemStr = tableElem[z-1]   # H has Z=1
    elif (11<=z<=18):
        tableElem = ['Na','Mg','Al','Si','P','S','Cl','Ar']  
        elemStr = tableElem[z-11]   # Na has Z=11  
    elif (19<=z<=36):
        tableElem = ['K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr']
        elemStr = tableElem[z-19]   # K has Z=19
    elif (37<=z<=54):    
        tableElem = ['Rb','Sr','Y','Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe']  
        elemStr = tableElem[z-37]   # Rb has Z=37
    elif (55<=z<=71):
        tableElem = ['Cs','Ba','La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu']  
        elemStr = tableElem[z-55]   # Cs has Z=55    
    elif (72<=z<=86):
        tableElem = ['Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn']  
        elemStr = tableElem[z-72]   # Hf has Z=72   
    elif (87<=z<=88):
        tableElem = ['Fr','Ra']  
        elemStr = tableElem[z-87]   # Fr has Z=87   
    elif (89<=z<=103):
        tableElem = ['Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm','Md','No','Fr']  
        elemStr = tableElem[z-89]   # Ac has Z=89   
    elif (104<=z<=118):
        tableElem = ['Rf','Db','Sg','Bh','Hs','Mt','Ds','Rg','Cn','Uut','Fl','Uup','Lv','Uus','Uuo']  
        elemStr = tableElem[z-104]   # Rf has Z=104 
    else:
        elemStr = 'N/A'   
    return elemStr
# --------------------------------------------------------------------------------------------------------
def printRandomSmily(z):
    # randomly print a smily on the console 
    smilyStr = ""    
    smilyStr = smilyStr + "                                                     ..::''''::..      \n "
    smilyStr = smilyStr + "                                           .:::.   .;''        ``;.    \n "
    smilyStr = smilyStr + "   ....                                    :::::  ::    ::  ::    ::   \n "
    smilyStr = smilyStr + " ,;' .;:                ()  ..:            `:::' ::     ::  ::     ::  \n "
    smilyStr = smilyStr + " ::.      ..:,:;.,:;.    .   ::   .::::.    `:'  :: .:' ::  :: `:. ::  \n "
    smilyStr = smilyStr + "  '''::,   ::  ::  ::  `::   ::  ;:   .::    :   ::  :          :  ::  \n "
    smilyStr = smilyStr + ",:';  ::;  ::  ::  ::   ::   ::  ::,::''.    .    :: `:.      .:' ::   \n "
    smilyStr = smilyStr + "`:,,,,;;' ,;; ,;;, ;;, ,;;, ,;;, `:,,,,:'   :;:    `;..``::::''..;'    \n "
    smilyStr = smilyStr + "                                                     ``::,,,,::''      \n "        
    if random.randint(1,100) == z:        
        print "\n" + smilyStr + '\n'
    else:
        return "Sorry, maybe next time."
# --------------------------------------------------------------------------------------------------------
def checkShellStr(shellConfStr,chkStr):
    if shellConfStr[-3] == chkStr:
        return 1
    else:
        return 0

# --------------------------------------------------------------------------------------------------------
def shellFilled(shellConfStr):
    if (int(shellConfStr[-2:]) > 0):
        return 1
    else:
        return 0
# --------------------------------------------------------------------------------------------------------
def incrementShell(shellStr):
    # print "Increment: " + shellStr
    ele = int(shellStr[-2:])
    newShellStr = shellStr[:-2] + "%02d" % (ele + 1)
    return newShellStr
# --------------------------------------------------------------------------------------------------------
def decrementShell(shellStr):
    # print "Decrement: " + shellStr
    ele = int(shellStr[-2:])
    if ele > 0:
        newShellStr = shellStr[:-2] + "%02d" % (ele - 1)
    else:
        print " ERROR 1234"
    return newShellStr
# --------------------------------------------------------------------------------------------------------

def excite(eleConfigStr, coreStr,valStr):
    
    # print eleConfigStr
    
    splitConf = eleConfigStr.split(' ')    # split at spaces, to separate shells
    splitConf = filter(None, splitConf)   # remove empty entries
    
    # print "c:"+coreStr
    # print "v:"+valStr
    # find index of shell block for core and valence
    for i in range(0,len(splitConf)):
        # print splitConf[i][:2]
        if (splitConf[i][:2] == coreStr):
            coreIdx = i
        elif (splitConf[i][:2] == valStr):
            valIdx = i
        # else:    
            # coreIdx = 0 
            # valIdx  = -1

    # print "c:%d" % coreIdx
    # print "v:%d" % valIdx
            
    if shellFull(splitConf[valIdx],splitConf[valIdx][-2:]):
        print "Valence Shell is full! What now?"
    else:
        newValStr = incrementShell(splitConf[valIdx])
        newCoreStr = decrementShell(splitConf[coreIdx])         # NOTE: No check, assuming core shell is always full
        
        # print "newCoreStr: " + newCoreStr
        # print "newValStr: " + newValStr

    # core is first, and valence is last shell (replace first and last withe excited config)
    if (coreIdx == 0) and (coreIdx < valIdx) and (valIdx == len(splitConf)-1):
        newEleConf = newCoreStr + " " + " ".join(splitConf[coreIdx+1:valIdx]) + " " + newValStr
        # newEleConf = "".join(newEleConf[0:valIdx]) + newValStr
        
    elif (coreIdx > 0) and (coreIdx < valIdx):
        newEleConf = " ".join(splitConf[0:coreIdx]) + " " + newCoreStr + " " + " ".join(splitConf[coreIdx+1:valIdx]) + " " + newValStr
    
    # print newEleConf
    
    return newEleConf
        # # print splitShell
        # thisShell = splitShell[0]
        # eleN = int(thisShell[-2:])                 # last 2 digits give number of electrons eleN
        # if (eleN > 0):
        #     newConf = thisShell[0:-2] + "%02d" % (eleN-1)
        # else:
        #     newConf = "ERRO"
        #
        #
 

# --------------------------------------------------------------------------------------------------------
def dropElectron(shellConfStr):  
    # ---------------------------------------------------------------------------------------
    splitShell = shellConfStr.split(' ')    # split at spaces, to separate shells
    splitShell = filter(None, splitShell)   # remove empty entries
    
    # simple case only one substring, decrement until zero (CHECK BEFORE calling this function!)
    if len(splitShell) == 1:
        # print splitShell
        thisShell = splitShell[0]
        eleN = int(thisShell[-2:])                 # last 2 digits give number of electrons eleN
        if (eleN > 0):
            newConf = thisShell[0:-2] + "%02d" % (eleN-1)
        else:
            newConf = "ERRO"

    # substring contains more than one ele config, needs rules to decrement electron
    elif len(splitShell) > 1:
        subIdx1 = -1                    # checking the last shell string first 
        subIdx2 = -2                    # minimum, if len > 1         
        subStr = splitShell[subIdx1]
        subsubStr = splitShell[subIdx2]
        
        # if last shell is an s or p shell empty this one first
        if checkShellStr(subStr,'s') or checkShellStr(subStr,'p'):
            # when sub p is filled -> decrement
            if shellFilled(subStr):   
                newSubShellStr = " " + dropElectron(subStr) + " "
                newConf = " ".join(splitShell[0:subIdx1]) + newSubShellStr

            # else (p is empty) check if subsub is filled -> decrement subsub          
            elif shellFilled(subsubStr):
                newSubShellStr = "" + dropElectron(subsubStr)  + " "
                # newConf = " ".join(splitShell[0:subIdx2]) + newSubShellStr + " ".join(splitShell[subIdx2+1:])
                newConf = " ".join(splitShell[0:subIdx2]) + newSubShellStr                                          # last cell should be empty, can be left out
            
            else:                
                print ' No applicable rule found. (Error 043)'            
                newConf = ' E043'       
        # ------------------------------------------------------------
        # if the last is a d then check for second last being and s shell, then remove from s, otherwise drop from the last d
        elif checkShellStr(subStr,'d'):
            # print subsubStr + " "+ subStr
            # if subSubStr (s) filled drop electron from this shell 
            if (checkShellStr(subsubStr,'s') and shellFilled(subsubStr)):
                newSubShellStr = "" + dropElectron(subsubStr) + " "             # NO SPACE in front of dopped subsubshell
                newConf = " ".join(splitShell[0:subIdx2]) + newSubShellStr + " ".join(splitShell[subIdx2+1:])
            
            # elif subSubStr is f orbital, and sub is non emty, then drop from subshell (d)    
            elif checkShellStr(subsubStr,'f') and shellFilled(subStr):
                newSubShellStr = " " + dropElectron(subStr) + " "               # BUT for subShell a space is needed before and after !!       
                newConf = " ".join(splitShell[0:subIdx1]) + newSubShellStr       

            # elif subSubStr is emty f orbital, then drop from subsubshell (d)    
            elif checkShellStr(subsubStr,'f') and shellFilled(subsubStr):
                newSubShellStr = "" + dropElectron(subsubStr) + " "              # NO SPACE in front of dopped subsubshell   
                newConf = " ".join(splitShell[0:subIdx2])  + newSubShellStr + " ".join(splitShell[subIdx2+1:])            
            
            # elif subSubStr (s) is empty, then drop from subshell (d)    
            elif not(shellFilled(subsubStr)) and shellFilled(subStr):
                newSubShellStr = " " + dropElectron(subStr) + " "
                newConf = " ".join(splitShell[0:subIdx1]) + newSubShellStr
                
            else:
                print ' No applicable rule found. (Error 023)'            
                newConf = ' E023'    
                
        # ------------------------------------------------------------
        # if the last is a f shell  then check for second last being and s or d shell, then remove from s/d, otherwise drop from the last f          
        elif checkShellStr(subStr,'f'):

            # when subsub is s shell
            if (checkShellStr(subsubStr,'s') and shellFilled(subsubStr) ):
                newSubShellStr = "" + dropElectron(subsubStr) + " "
                newConf = " ".join(splitShell[0:subIdx2]) + newSubShellStr + " ".join(splitShell[subIdx2+1:])
            
            # when subsub is d shell
            elif (checkShellStr(subsubStr,'d') and shellFilled(subsubStr) ):
                newSubShellStr = "" + dropElectron(subsubStr) + " "
                newConf = " ".join(splitShell[0:subIdx2]) + newSubShellStr + " ".join(splitShell[subIdx2+1:])
                            
            # when subsub is empty, but sub is filled
            elif (not(shellFilled(subsubStr)) and shellFilled(subStr) ):                
                newSubShellStr = " " + dropElectron(subStr) + " "
                newConf = " ".join(splitShell[0:subIdx1]) + newSubShellStr
            
            else:
                print ' No applicable rule found. (Error 022)'            
                newConf = ' E022'            
                
        else:
            print ' No applicable rule found. (Error 099)'            
            newConf = ' E099'  
        
    else:
        print ' No applicable rule found. (Error 042)'            
        newConf = ' E042'  
         
    return newConf

# --------------------------------------------------------------------------------------------------------
def oxide(z, level):
    lvl = level 
    if (0 <= z-lvl):  
        print "********** Removing %d electron(s) from %s  (Z = %d) ******** " %  (lvl, zToElem(z), z)    
        eleConfStr = oxideConf(z, level)
        eleConfStr = zToElem(z) + "%d" % level + "+  " + eleConfStr
        
    return eleConfStr

# --------------------------------------------------------------------------------------------------------
def oxideConf(z, level):

    lvl = level 
    if (0 <= z-lvl):          
        if ((z == 1) and (lvl == 1)) or ((z == 2) and (lvl == 2)) or (0 == z-lvl):
            eleConfStr = "1s00"                                                             
        elif (z == 2) and (lvl == 1):                                                       
            eleConfStr = "1s01"                                                             
        elif (z <= 20) and (0 < z-lvl) :                                                   
            eleConfStr = getEleConf(z - level)                                              
        else:                                                                  
            eleConfStr = getEleConf(z)
            # print eleConfStr
            # -------------------------------
            while (lvl > 0) and len(eleConfStr.split()) >= 2:
                lstConf = eleConfStr.split()         # IMPORTANT LINE!!! Otherwise change is NOT APPLIED !!!! DO NOT COMMENT OUT AGAIN!!! 
                subIdx1 = -1
                subIdx2 = -2
                subStr = lstConf[subIdx1]
                subsubStr = lstConf[subIdx2]

                # if the last is a non empty s or p orbital then drop electron
                if (checkShellStr(subStr,'s') or checkShellStr(subStr,'p')) and shellFilled(subStr):
                    newSubShellStr = " " + dropElectron(subStr)    #+ " "
                    eleConfStr = " ".join(lstConf[0:subIdx1]) + newSubShellStr
                   
                # if the last is a d or f shell then hand over last two shells for decrement of electron           
                # elif checkShellStr(subStr,'d') or checkShellStr(subStr,'f'):
                elif shellFilled(subStr) or shellFilled(subsubStr):
                    lastTwo = " ".join(lstConf[subIdx2:])                       # create substring with last two shells
                    newSubShellStr = " " + dropElectron(lastTwo)   #+ " "
                    eleConfStr = " ".join(lstConf[0:subIdx2]) + newSubShellStr
                
                # check 3rd shell from the end
                elif shellFilled(lstConf[-3]):
                    newSubShellStr = " " + dropElectron(lstConf[-3]) + " "
                    # eleConfStr = " ".join(lstConf[0:-3]) + newSubShellStr  + " ".join(lstConf[-3+1:])
                    eleConfStr = " ".join(lstConf[0:-3]) + newSubShellStr  + " ".join(lstConf[-3+1:-1])      # take only the 2nd last, the last is left out (the TWO last are empty here)
                    
                else:
                    print ' No applicable rule found. (Error 061)'            
                    eleConfStr = ' E061' 
            
                lvl = lvl - 1            
            # --- END WHILE -------------------------------------------------------------------------------------
    # else -> (z-lvl=0)
    else:    
        print "********** Shell(s) empty! Cannot remove %d electron(s) from %s.  ********** " % (level, zToElem(z))
        eleConfStr = " ???? Are you sure you know what you are doing ????"
    
    # ----------------------------------------------------------
    # print "New Subshell: " + newSubShellStr
    # print ">>" + " ".join(lstConf[-subIdx:])
    # ----------------------------------------------------------
    
    # ----------------------------------------------------------
    # eleConfStr = relaxOxideConf(eleConfStr)
    # ----------------------------------------------------------
    
    return eleConfStr
# --------------------------------------------------------------------------------------------------------
def relaxOxideConf(eleConfStr):

    lstConf = eleConfStr.split()         # IMPORTANT LINE!!! Otherwise change is NOT APPLIED !!!! DO NOT COMMENT OUT AGAIN!!! 
    print lstConf
    if len(lstConf) >= 2:    
        subIdx1 = -1
        subIdx2 = -2
        subStr = lstConf[subIdx1]
        subsubStr = lstConf[subIdx2]
        
        print subStr
        print subsubStr
        
        # if the last is a non empty s or p orbital then drop electron
        if shellFilled(subStr):
            # if the last is a non-empty s shell
            if ( checkShellStr(subStr,'s') and shellFilled(subStr) ):
                
                print "filled S-Shell: " + subStr
                
            elif ( checkShellStr(subsubStr,'s') and shellFilled(subsubStr) ):
                
                print "filled S-Shell: " + subsubStr
            




    # # if the last is a non empty s or p orbital then drop electron
    # if (checkShellStr(subStr,'s') or checkShellStr(subStr,'p')) and shellFilled(subStr):
    #     newSubShellStr = " " + dropElectron(subStr)    #+ " "
    #     eleConfStr = " ".join(lstConf[0:subIdx1]) + newSubShellStr
    
    
    
    
   
   # lstConf = list(eleConfStr)
   # lstConf[-7] = '1'
   # eleConfStr = "".join(lstConf)
    
    
    
    
    
    
    return eleConfStr






# --------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------
def getEleConf(z):   
    shell1 = ['s']
    shell2 = ['s','p']
    shell3 = ['s','p']
    shell4 = ['s','d','p']
    shell5 = ['s','d','p']
    shell6 = ['s','f','d','p']
    shell7 = ['s','f','d','p']
    shell8 = ['s','g','f','d','p']
    shell9 = ['s','g','f','d','p']
    shell10 = ['s','h','g','f','d','p']   
    shells   = [shell1,shell2,shell3,shell4,shell5,shell6,shell7,shell8,shell9,shell10]
    ele = z
    eleConfStr = ''

    main  = 0
    shell = 0
    eleShell = 0

    shellStr = shells[main][shell]          # only needed when shell empty (ele == 0)
    while ele > 0:
        ele = ele - 1
        eleShell = eleShell + 1
        shellStr = shells[main][shell]   
     
        if shellFull( shellStr, eleShell):
            eleConfStr = eleConfStr + getConfigString(main,shells,shell,shellStr,eleShell)        
            if shell < len(shells[main])-1:
                shell = shell + 1
                # print "full.shell: %d" % shell
                eleShell = 0        
            else:
                main = main + 1
                # print "full.main: %d" % main
                shell = 0
                eleShell = 0
    # ----- END WHILE ----------------------------------------------------------------            
    
    # --------------------------------------------------------------------------------
    # add last partially filled shell (AND EXCEPTIONS), if not full and nonzero eleShell left over    (chekcing exception here too)         
    # --------------------------------------------------------------------------------
    if (shellFull(shellStr, eleShell) != 1) and (eleShell != 0):
        # exception get an ADDITIONAL 5d1 or 6d1 term
        excepZ = [57,58, 64, 89,90,91,92,93,96, 103]                             # if ((57<=z<=58) or (89<=z<=90) or (z==64) or (z==96)):
        if ( (z in excepZ) > 0):
            if (z==90):
                # insert the 5d1 string
                eleShell = eleShell - 2    # decrement f shell
                eleConfStr = eleConfStr + "%d" % main + "d02 "                
            else:
                # insert the 5d1 string
                eleShell = eleShell - 1    # decrement f shell
                eleConfStr = eleConfStr + "%d" % main + "d01 "

            if eleShell > 0:
                eleConfStr = eleConfStr + getConfigString(main,shells,shell,shellStr,eleShell)

        # check if full d shell can be made, when previous shell is a full s shell, for d4 preference over d5 by using s1 instead of an s2 config
        # SHIFT 1 electon from s2 shell to dN+1 shell (for Z = list, and 24,29, 42,47,79
        elif ((z in [41,44,45,47]) > 0) or ( ( ( ((eleShell == 4) or (eleShell == 9)) and (main < 5)) and (shellStr == 'd')) and ((eleConfStr[-4] == 's') and (eleConfStr[-2] == '2' )) ):
            eleShell = eleShell + 1    # increment from d4 to d5, or d9 to d10 on the expense of an s shell electron (s1 instead of s2)
            lstConf = list(eleConfStr)
            lstConf[-2] = '1'          # replacing the s2 with s1 (position check above with eleConfStr[-4] == 's'...)
            eleConfStr = "".join(lstConf)
            eleConfStr = eleConfStr + getConfigString(main,shells,shell,shellStr,eleShell) 
        
        # special case Z=46 (according to wikipedia)
        elif (z == 46):
            eleShell = eleShell + 2    # increment from d4 to d5, or d9 to d10 on the expense of an s shell electron (s1 instead of s2)
            lstConf = list(eleConfStr)
            lstConf[-2] = '0'          # replacing the s2 with s1 (position check above with eleConfStr[-4] == 's'...)
            eleConfStr = "".join(lstConf)
            eleConfStr = eleConfStr + getConfigString(main,shells,shell,shellStr,eleShell) 
                                                  
        # check if full d shell can be made, when previous shell is a full s shell        
        elif (z == 78) or ((z != 111) and ( ((eleShell == 9) and (4 < main )) and (shellStr == 'd')) and ((eleConfStr[-9] == 's') and (eleConfStr[-7] == '2' ))):
            eleShell = eleShell + 1    # increment from d9 to d10, for higher shells (row >=6)
            lstConf = list(eleConfStr)
            lstConf[-7] = '1'
            eleConfStr = "".join(lstConf)
            eleConfStr = eleConfStr + getConfigString(main,shells,shell,shellStr,eleShell)                
        else:
            eleConfStr = eleConfStr + getConfigString(main,shells,shell,shellStr,eleShell) 
    
    return eleConfStr

# --------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------
def help():
    print " ==============================================================================================          "
    print " ========================================== Mini Help =========================================          "
    print " At the python prompt after loading the module with:                                                      "
    print " \t import EleConfig                                                                                     "
    print " you can use the following  with z<119                                                                   "
    print " \t EleConfig.zToElem(z)           \t\t  gives just the label for a given z<119                          "
    print " \t EleConfig.getEleConf(z)        \t\t  get electron config for 0 < z < 119 (only electron config)      "
    print " \t EleConfig.getZ(z)              \t\t  get electron config for 0 < z < 119 (Label and electron config) "
    print " \t EleConfig.getElement('He')     \t\t  same as getZ(z), but for labels (He, Sc,Ti, Mo, Ag,.. )         "
    print " \t EleConfig.oxide(z, valence)    \t\t  get oxide; for z=26, valence=2 Fe2+                             "
    print " \t EleConfig.oxideConf(z, valence)\t\t  similar to oxide (only electron config)                             "
    print " \t EleConfig.dropElectron('3d10') \t\t  method used to reduce electron count (accepts strings)          "
    print " \t EleConfig.printRandomSmily(z)  \t\t  random smily                                                    "
    print " \t EleConfig.excite(EleConfig.getEleConf(22), '1s','3d')                                                                 "
    print " ==============================================================================================          "
    print " ==============================================================================================          "

    
# --------------------------------------------------------------------------------------------------------
def getElement(zStr):  
    for k in range(1,119):
        if zStr == zToElem(k):
            return getZ(k)
    
    
# --------------------------------------------------------------------------------------------------------
def getZ(z):  
    
    # --------------------------------------------------------------
    # ONLY Z <=118 are implemented 
    if z <= 118:
        
        print "******** Calculating electron configuration for " + zToElem(z) +  "  (Z = %d)" % z + " ******** "
      
        eleConfStr = getEleConf(z)
        # ----------------------------------
        printRandomSmily(z)
        # ----------------------------------    
        return zToElem(z) + "  " + eleConfStr
        
    # --------------------------------------------------------------
    else:
        print "******** Sorry, I know only the elements with Z < 119. Z=%d" % z + " is out of my...  ******** "
        print getSmilyStr() 
        
        return "\n" + "\n"     
    # --------------------------------------------------------------        
        
# --------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------
