# ------------------------------------------------------------------------------------------------------------------------------------
# Rules defined according to:  https://en.wikipedia.org/wiki/Electron_configuration#Atoms:_Aufbau_principle_and_Madelung_rule
# Extendes Table used for names: https://en.wikipedia.org/wiki/Extended_periodic_table
# Oxidation states follow the rule: first remove s or p electron, otherwise decrement d or s
# 
# ------------------------------------------------------------------------------------------------------------------------------------
# ========== Example Code: ==========

import EleConfig as ec


# -----------------------------------------------------------------------------
# choose a testmode HERE or see the code samples below for usuage examples
testMode = 3    # testMode 0,1,2,3, else

# ----------------------------------------------------
# ----------------------------------------------------
if testMode == 0:
    
    # ec.help()
 
    # print ec.getZ(22)
    # print ec.getElement("He")
    # print ec.oxide(22, 2)

    print ec.getZ(22)
    print " "
    print "1sXAS: " + ec.excite(ec.getEleConf(22), '1s','3d')
    print "2sXAS: " + ec.excite(ec.getEleConf(22), '2s','3d')
    print "     "
    print ec.oxide(22, 4)
    print "1sXAS: " + ec.excite(ec.oxideConf(22,4), '1s','3d')
    print "2sXAS: " + ec.excite(ec.oxideConf(22,4), '2s','3d')    
    # print "     "
    # print ec.oxide(22, 5)
    # print "1sXAS: " + ec.excite(ec.oxideConf(22,5), '1s','3d')
    # print "2sXAS: " + ec.excite(ec.oxideConf(22,5), '2s','3d')


# ----------------------------------------------------
# ec.getZ(z) returns electron configuration for 'z' as string
elif testMode == 1:
    for z in range(21,30):
        print ec.getZ(z)
    

# ----------------------------------------------------
# ec.oxide(z, valence) returns electron configuration for 'z' with oxidation state 'valence' as string
elif testMode == 2:
    z = 24          # Z = 26 Fe

    print " "
    print ec.getZ(z)
    for valence in range(1,9):
        print ec.oxide(z, valence)

    # print ec.oxide(z, 8)
    
# ----------------------------------------------------
elif testMode == 3:
    for z in range(19,31):
        print " "
        print ec.getZ(z)
        for valence in range(1,9):
            print ec.oxide(z, valence)


# ----------------------------------------------------
# other implented subroutines 
else:
    z = 33
    print ec.printRandomSmily(z)                                                # random smily
        
    print "The element corresponding to Z=26 is  " + ec.zToElem(z)             # get name string for given Z
    print "The electron configuration of Z=26 is " + ec.getEleConf(z)          # get GS electron configuration for z  
      
    print " Two level electron drop if at least one is non empty:"
    print " 4d04 -> " + ec.dropElectron(' 4d04 ')                               # drop an electron from a config string
    print ' 3d10  -> ' + ec.dropElectron('3d10')
    
    print ' 3d10 4s02  -> ' + ec.dropElectron('3d10 4s02')                      # drop an electron from a config string with 2 subshells (2 is max at the moment) 
    print ' 5s02 5p04  -> ' + ec.dropElectron('5s02 5p04 ')
    
    
    
   