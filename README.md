# PeriodicTable_ElectronicConfiguration

A Simple Python Script to compute electronic configurations

This is a script that I created during the time where I studied physics. It computes the electron configuration based on the Aufbauprinciple and Madelung rule: 
https://en.wikipedia.org/wiki/Electron_configuration#Atoms:_Aufbau_principle_and_Madelung_rule

Nothing super fancy, but maybe useful for some students or just as a playground.

On some general instructions on how to use it see the wiki:
https://gitlab.com/PatZim/periodictable_electronicconfiguration/wikis/home

---------------------------------------------------------------------------------------------------------------------------------

The code is provided as is, use at your own risk.